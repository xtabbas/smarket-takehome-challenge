import React, { useState, useEffect } from "react";
import { usePostActivities, useUpdateActivities } from "../network/activities";
import { useGetActivities } from "../network/activities";

export default function CreateActivityModal({
  subtitle,
  closeModal,
  innerRef,
  activityToEdit,
  edit,
  postSchedule,
}) {
  const {
    mutate: createActivity,
    isLoading,
    error,
    isError,
    isSuccess,
  } = usePostActivities(postSchedule, closeModal);

  const updateActivity = useUpdateActivities();
  const allActivities = useGetActivities();

  const [name, setName] = useState("");
  const [interval, setInterval] = useState("");
  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");

  useEffect(() => {
    if (activityToEdit && edit) {
      // console.log(
      //   "in update activity effect",
      //   allActivities.data,
      //   activityToEdit
      // );

      if (allActivities) {
        const filtered = allActivities.data.filter(
          (a) => a._id === activityToEdit.activity_id
        )[0];

        if (filtered) {
          const todayStart = new Date();

          todayStart.setHours(startTime.split(":")[0]);
          todayStart.setMinutes(startTime.split(":")[1]);

          const todayEnd = new Date();
          todayEnd.setHours(endTime.split(":")[0]);
          todayEnd.setMinutes(endTime.split(":")[1]);

          setName(filtered.activity_name);
          setInterval(filtered.consistency_preference.prefered_interval);
          setStartTime(
            `${(
              "0" +
              new Date(filtered.consistency_preference.start_time).getHours()
            ).slice(-2)}:${new Date(
              filtered.consistency_preference.start_time
            ).getMinutes()}`
          );
          setEndTime(
            `${(
              "0" +
              new Date(filtered.consistency_preference.end_time).getHours()
            ).slice(-2)}:${new Date(
              filtered.consistency_preference.end_time
            ).getMinutes()}`
          );
        }
      }
    }
  }, [activityToEdit, edit]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      {isLoading ? (
        "Adding todo..."
      ) : (
        <>
          {isError ? (
            <div>An error occurred: {error.response.data.message}</div>
          ) : null}

          {isSuccess ? <div>Todo added!</div> : null}

          <h2 ref={innerRef}>Create activity</h2>
          <div>Is there anything you want to be consistent with?</div>
          <form
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            activity name
            <input
              value={name}
              style={{ width: 250, height: 30 }}
              onChange={(e) => setName(e.target.value)}
            />
            prefered interval
            <input
              value={interval}
              type="number"
              style={{ width: 250, height: 30 }}
              onChange={(e) => setInterval(e.target.value)}
            />
            prefered start time
            <input
              value={startTime}
              type="time"
              style={{ width: 250, height: 30 }}
              onChange={(e) => setStartTime(e.target.value)}
            />
            prefered end time
            <input
              value={endTime}
              type="time"
              style={{ width: 250, height: 30 }}
              onChange={(e) => setEndTime(e.target.value)}
            />
          </form>
          <button
            onClick={() => {
              //create activity
              if ((name, interval, startTime, endTime)) {
                if (!name || name === "") {
                  console.log("no name :(");
                  return;
                }
                if (!interval || interval === "") {
                  console.log("no interval :(");
                  return;
                }
                if (!startTime || startTime === "") {
                  console.log("no startTime :(");
                  return;
                }
                if (!endTime || endTime === "") {
                  console.log("no endTime :(");
                  return;
                }
              }

              //   console.log("Start Time", startTime);

              const todayStart = new Date();

              todayStart.setHours(startTime.split(":")[0]);
              todayStart.setMinutes(startTime.split(":")[1]);

              const todayEnd = new Date();
              todayEnd.setHours(endTime.split(":")[0]);
              todayEnd.setMinutes(endTime.split(":")[1]);
              //   today.setHours(endTime.split(":")[0]);
              // .setMinutes(endTime.split(":")[1]);

              const activity = {
                activityName: name,
                preferedInterval: interval,
                preferedStartTime: todayStart,
                preferedEndTime: todayEnd,
              };

              if (activity && edit) {
                updateActivity.mutate({
                  activity,
                  _id: activityToEdit.activity_id,
                });
              } else {
                createActivity(activity);
              }
            }}
          >
            {activityToEdit && edit ? "update!" : "create!"}
          </button>
          {edit && <button onClick={closeModal}>close</button>}
        </>
      )}
    </>
  );
}
