import React, { useEffect, useState } from "react";
import DateTime from "../utils/useDate";
import Modal from "react-modal";
import {
  useGetSchedule,
  useDeleteSchedule,
  useUpdateSchedule,
  usePostSchedule,
} from "../network/schedules";
import { useMutation } from "react-query";
import { useGetAnalytics } from "../network/analytics";

Modal.setAppElement("#root");
const customStyles = {
  content: {
    // top: "50%",
    // left: "50%",
    // right: "auto",
    // bottom: "auto",
    // marginRight: "-50%",
    // transform: "translate(-50%, -50%)",

    boxSizing: "border-box",
    width: "760px",
    // boxSizing: "0em",
    maxWidth: "100%",
    margin: "auto",
    padding: "24px",
    borderTopLeftRadius: "16px",
    borderTopRightRadius: "16px",
    borderBottomRightRadius: "16px",
    borderBottomLeftRadius: "16px",
    /* border: none, */
    // background: "#000",
    backgroundColor: "hsl(209, 35%, 11.5%)",
    /* box-shadow: none, */
    position: "relative",
    flex: "0 1 auto",
  },
  overlay: {
    position: "fixed",
    top: "0px",
    left: "0px",
    width: "100%",
    height: "100%",
    background: "hsl(209, 35%, 11.5%, 70%)",
    overflowY: "scroll",
    zIndex: "11",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: "18px",
    boxSizing: "border-box",
    pointerEvents: "unset",
    opacity: "1",
    transform: "none",
    transformOrigin: "50% 50% 0px",
  },
};

const activeStyle = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "column",
  margin: "20px",
  padding: "20px",
  border: "1px solid grey",
  height: "75vh",
  minHeight: "75vh",
  width: "20vw",
  minWidth: "20vw",
};

export default function Schedule({
  isToday,
  children,
  date,
  activities,
  active,
  isPostingSchedule,
  postSchedule,
  setCreateActivity,
}) {
  const {
    data: schedule,
    status,
    error,
    isLoading,
    isError,
    isSuccess,
    refetch,
    isIdle,
    remove,
  } = useGetSchedule(date);

  const deleteActivity = useDeleteSchedule(date);
  const updateSchedule = useUpdateSchedule();

  // console.log("date in schedule component", isLoading);

  useEffect(() => {
    if (active && !isPostingSchedule) {
      refetch();
    }
    // remove();
  }, [isPostingSchedule, active]); // eslint-disable-line react-hooks/exhaustive-deps

  // useEffect(
  //   () => {
  //     if (isToday) {
  //       // console.log("inside", new Date(date).toDateString());

  //       let newd = new Date(date).toDateString();

  //       postSchedule(newd);
  //       // refetch();
  //     }
  //   },
  //   [] // eslint-disable-line react-hooks/exhaustive-deps
  // );

  const today = new Date();
  const daysPassedSinceToday = DateTime.utc(date).minus(today);
  const isRequestingForPast = daysPassedSinceToday < 0;

  // console.log("insdie", schedule);

  let mainSchedule = [];
  let reasons = [];
  let scheduleContainer = {};
  if (schedule) {
    if (schedule.schedule) {
      if (schedule.schedule.length > 0) {
        scheduleContainer = schedule.schedule[0];
        mainSchedule = schedule.schedule[0].schedule;
        reasons = schedule.schedule[0].reasons;
      }
    }
  }
  // console.log("insdie", mainSchedule);

  const [editor, setEditor] = useState({
    startTime: "",
    endTime: "",
    scheduleId: "",
    activityId: "",
    suggestionId: "",
    modalIsOpen: false,
    activity: {},
  });

  const [activityAnalytics, setActivityAnalytics] = useState({
    activity: {},
    modalIsOpen: false,
  });

  // console.log("Editor", getAnalytics.data);

  const getAnalytics = useGetAnalytics(activityAnalytics.activity.activity_id);

  useEffect(() => {
    // console.log("in effect", activityAnalytics);
    if (
      activityAnalytics.modalIsOpen &&
      activityAnalytics.activity.activity_id
    ) {
      getAnalytics.refetch();
    }
  }, [activityAnalytics.modalIsOpen]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="schedule" style={isToday ? activeStyle : {}}>
      {children}
      {isLoading ? (
        "Fetching your schedule :)"
      ) : (
        <>
          {isError ? <div>An error occurred: {error.message}</div> : null}

          {isSuccess && mainSchedule.length > 0 ? (
            <>
              {mainSchedule
                .reduce(
                  (acc, val, i, arr) => {
                    // start with "free time"

                    // we have 24 hours in a day
                    // activity has a end_time 2pm
                    // before 2pm we have free time
                    // after 2pm we have free time
                    // we need to add this to array
                    // console.log("i & acc[i]", i, acc[i]);
                    return [...acc, val, { free: true }];

                    // if (acc[i].free) {
                    //   // if prev was free add it
                    //   return [...acc, val];
                    // } else {
                    //   // otherwise (add free?) but how would we know...
                    //   return [...acc, { free: true }];
                    // }
                  },
                  [{ free: true }]
                )
                .map((s) => {
                  //   console.log("s", s);

                  //   if (!s.free) {
                  //     if (s.time_has_passed && s.status === "UNDETERMINED") {
                  //       console.log(
                  //         "HAVE YOU DONE THIS ACTIVITY?",
                  //         s.activity_name
                  //       );
                  //       // add to a list that show modal if items in array
                  //     } else {
                  //       console.log("KEEP DOING THIS ACTIVITY!", s.activity_name);
                  //     }
                  //   }

                  if (s.free) {
                    return (
                      <div>
                        {/* You should not do any of the tracked activity in this
                        period / today for maximizing consistency. You could
                        schedule one time events in this time. */}
                        free time (for oncies)
                      </div>
                    );
                  } else {
                    return (
                      <div style={{ border: "1px solid grey" }}>
                        <p>{s.activity_name}</p>
                        <p>
                          {new Date(s.start_time).toLocaleTimeString("en-US", {
                            hour: "2-digit",
                            minute: "2-digit",
                          })}{" "}
                          →
                          {new Date(s.end_time).toLocaleTimeString("en-US", {
                            hour: "2-digit",
                            minute: "2-digit",
                          })}
                        </p>
                        <p>{s.status}</p>
                        <p>{s.reason}</p>
                        <p>committed: {s.committed ? "true" : "false"}</p>
                        <p>if you do it consistency score will become:</p>
                        <button
                          onClick={() => {
                            // console.log("delete", scheduleContainer, s);
                            deleteActivity.mutate({
                              scheduleId: scheduleContainer._id,
                              activityId: s.activity_id,
                              force: true,
                            });
                          }}
                        >
                          delete
                        </button>
                        <button
                          onClick={() => {
                            // console.log("delete", scheduleContainer, s);
                            updateSchedule.mutate({
                              scheduleId: scheduleContainer._id,
                              activityId: s.activity_id,
                              suggestionId: s._id,
                              committed: !s.committed,
                            });
                          }}
                        >
                          commit
                        </button>
                        <button
                          onClick={() => {
                            // console.log("delete", scheduleContainer, s);
                            updateSchedule.mutate({
                              scheduleId: scheduleContainer._id,
                              activityId: s.activity_id,
                              suggestionId: s._id,
                              status: "YES",
                            });
                          }}
                        >
                          done
                        </button>
                        <button
                          onClick={() => {
                            // console.log("delete", scheduleContainer, s);
                            // open activity modal with update somehow
                            setEditor({
                              ...editor,
                              modalIsOpen: true,
                              scheduleId: scheduleContainer._id,
                              activityId: s.activity_id,
                              suggestionId: s._id,
                              activity: s,
                              startTime: `${(
                                "0" + new Date(s.start_time).getHours()
                              ).slice(-2)}:${new Date(
                                s.start_time
                              ).getMinutes()}`,
                              endTime: `${(
                                "0" + new Date(s.end_time).getHours()
                              ).slice(-2)}:${new Date(
                                s.end_time
                              ).getMinutes()}`,
                            });
                          }}
                        >
                          edit
                        </button>
                        <button
                          onClick={() => {
                            // console.log("delete", scheduleContainer, s);
                            // open activity modal with update somehow
                            setActivityAnalytics({
                              ...activityAnalytics,
                              modalIsOpen: true,
                              activity: s,
                            });
                          }}
                        >
                          analytics
                        </button>
                      </div>
                    );
                  }
                })}
            </>
          ) : (
            <>
              nothing to do today
              {reasons.map((reason) => (
                <div>
                  {reason.activity_name}
                  {reason.determination}
                  {reason.reason}
                </div>
              ))}
            </>
          )}
        </>
      )}

      {mainSchedule.length < 1 && !isRequestingForPast ? (
        <button onClick={() => postSchedule(date)}>
          Get Schedule In Advance
        </button>
      ) : null}

      {isPostingSchedule && <div>Thinking for suggestions!!!</div>}

      <Modal
        isOpen={editor.modalIsOpen}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <>
          {updateSchedule.isLoading ? (
            "Adding todo..."
          ) : (
            <>
              {updateSchedule.isError ? (
                <div>
                  An error occurred:{" "}
                  {updateSchedule.error.response.data.message}
                </div>
              ) : null}

              {updateSchedule.isSuccess ? <div>Todo added!</div> : null}

              <h2>Edit activity: {editor.activity.activity_name}</h2>
              <form
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                start time
                <input
                  value={editor.startTime}
                  type="time"
                  style={{ width: 250, height: 30 }}
                  onChange={(e) =>
                    setEditor({ ...editor, startTime: e.target.value })
                  }
                />
                end time
                <input
                  value={editor.endTime}
                  type="time"
                  style={{ width: 250, height: 30 }}
                  onChange={(e) =>
                    setEditor({ ...editor, endTime: e.target.value })
                  }
                />
              </form>
              <button
                onClick={() => {
                  //create activity
                  if ((editor.startTime, editor.endTime)) {
                    if (!editor.startTime || editor.startTime === "") {
                      // console.log("no startTime :(");
                      return;
                    }
                    if (!editor.endTime || editor.endTime === "") {
                      // console.log("no endTime :(");
                      return;
                    }
                  }

                  const todayStart = new Date(editor.activity.start_time);

                  todayStart.setHours(editor.startTime.split(":")[0]);
                  todayStart.setMinutes(editor.startTime.split(":")[1]);

                  const todayEnd = new Date(editor.activity.end_time);
                  todayEnd.setHours(editor.endTime.split(":")[0]);
                  todayEnd.setMinutes(editor.endTime.split(":")[1]);
                  //   today.setHours(endTime.split(":")[0]);
                  // .setMinutes(endTime.split(":")[1]);

                  updateSchedule.mutate({
                    scheduleId: editor.scheduleId,
                    activityId: editor.activityId,
                    suggestionId: editor.suggestionId,
                    startTime: todayStart,
                    endTime: todayEnd,
                  });

                  setEditor({ ...editor, modalIsOpen: false });
                }}
              >
                update
              </button>
              <button
                onClick={() => setEditor({ ...editor, modalIsOpen: false })}
              >
                close
              </button>
            </>
          )}
        </>
      </Modal>

      <Modal
        isOpen={activityAnalytics.modalIsOpen}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <>
          {getAnalytics.isLoading ? (
            "getting analytics..."
          ) : (
            <>
              {getAnalytics.isError ? (
                <div>
                  An error occurred: {getAnalytics.error.response.data.message}
                </div>
              ) : null}

              <h2>
                Activity Analytics: {activityAnalytics.activity.activity_name}
              </h2>

              {getAnalytics.isSuccess ? (
                <div>
                  <h3>Interval consistencies</h3>
                  <h4>{`Average score: ${
                    getAnalytics.data.analytics[
                      getAnalytics.data.analytics.length - 1
                    ].average_consistency
                  }`}</h4>
                  {getAnalytics.data.analytics.map((analytic, index) => (
                    <div>
                      {!analytic.average_consistency.includes(
                        "recalibrating"
                      ) && (
                        <div>
                          {`Date Recorded: ${new Date(
                            analytic.date_recorded
                          ).toLocaleDateString()}`}
                          <br />
                          {`Prefered interval when recorded: ${analytic.prefered_interval_when_recorded}`}
                          <br />
                          {`Consistency Score: ${analytic.interval_consistency.consistency_score}`}
                          <br />
                          {`SD: ${analytic.interval_consistency.standard_deviation}`}
                          <br />

                          <a>
                            Cluster
                            {analytic.interval_consistency.dates_cluster.map(
                              (c) => (
                                <div>
                                  {new Date(c.date).toLocaleDateString()}
                                </div>
                              )
                            )}
                          </a>
                        </div>
                      )}
                      {index !== getAnalytics.data.analytics.length - 1 && (
                        <div>----Anamoly occured----</div>
                      )}
                    </div>
                  ))}
                </div>
              ) : null}

              <button
                onClick={() =>
                  setActivityAnalytics({
                    ...activityAnalytics,
                    modalIsOpen: false,
                  })
                }
              >
                close
              </button>

              <button onClick={() => getAnalytics.refetch()}>refetch</button>
            </>
          )}
        </>
      </Modal>
    </div>
  );
}
