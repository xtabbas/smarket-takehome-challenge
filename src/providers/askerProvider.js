import React, { createContext, useMemo, useContext, useState } from "react";
import { useQuery } from "react-query";
import Modal from "react-modal";
import { useMutation, useQueryClient } from "react-query";
import { useAxios } from "./axiosProvider";
import { useUpdateSchedule } from "../network/schedules";

export const AskerContext = createContext(undefined);

export function AskerProvider({ children }) {
  const [ask, setAsk] = useState([]);
  // const ask = [];
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  function pop() {
    ask.pop();
    // const popped = ask;
    // popped.pop();
    setAsk(ask);

    if (ask.length < 1 && modalIsOpen) {
      // show modal
      closeModal();
    }
  }

  // console.log("askaskask", ask);
  function push(value) {
    let hasIt = ask.filter((f) => f._id === value._id)[0];

    if (!hasIt) {
      ask.push(value);
      setAsk(ask);
    }

    if (ask.length > 0 && !modalIsOpen) {
      // show modal
      openModal();
    }
  }

  const updateSchedule = useUpdateSchedule(() => {
    pop();
  });

  return (
    <AskerContext.Provider
      value={{
        push: push,
        pop: pop,
      }}
    >
      {children}
      <Modal
        isOpen={modalIsOpen}
        // onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        {modalIsOpen &&
          `HAVE YOU DONE ${ask[0] && ask[0].activity_name.toUpperCase()}`}
        {modalIsOpen &&
          ` ON ${ask[0] && new Date(ask[0].end_time).toLocaleString()} ??`}

        <button
          onClick={() => {
            // pop();
            // console.log("hello world");
            updateSchedule.mutate({
              activityId: ask[0].activity_id, // activity 1
              scheduleId: ask[0].schedule_id,
              suggestionId: ask[0]._id,
              status: "YES",
            });
            // {
            //    activityId: schedule_activity_id, // activity 1
            //    scheduleId: schedule_id,
            //    status: "PARTIALLY",
            //    startTime: newStartTime,
            //    endTime: newEndTime,
            // }
          }}
        >
          yes
        </button>
      </Modal>
    </AskerContext.Provider>
  );
}

export function useAsker() {
  return useContext(AskerContext);
}

const customStyles = {
  content: {
    // top: "50%",
    // left: "50%",
    // right: "auto",
    // bottom: "auto",
    // marginRight: "-50%",
    // transform: "translate(-50%, -50%)",

    boxSizing: "border-box",
    width: "760px",
    // boxSizing: "0em",
    maxWidth: "100%",
    margin: "auto",
    padding: "24px",
    borderTopLeftRadius: "16px",
    borderTopRightRadius: "16px",
    borderBottomRightRadius: "16px",
    borderBottomLeftRadius: "16px",
    /* border: none, */
    // background: "#000",
    backgroundColor: "hsl(209, 35%, 11.5%)",
    /* box-shadow: none, */
    position: "relative",
    flex: "0 1 auto",
  },
  overlay: {
    position: "fixed",
    top: "0px",
    left: "0px",
    width: "100%",
    height: "100%",
    background: "hsl(209, 35%, 11.5%, 70%)",
    overflowY: "scroll",
    zIndex: "11",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: "18px",
    boxSizing: "border-box",
    pointerEvents: "unset",
    opacity: "1",
    transform: "none",
    transformOrigin: "50% 50% 0px",
  },
};
