import React, { createContext, useMemo, useContext } from "react";
import Axios from "axios";
import { useQuery } from "react-query";

export const AxiosContext = createContext(undefined);

export function AxiosProvider({ children }) {
  //   const axios = useMemo(() => {
  const axios = Axios.create({
    headers: {
      "Content-Type": "application/json",
    },
  });

  //   console.log("Calling axios provider!!");

  axios.interceptors.request.use(async (config) => {
    // Read token for anywhere, in this case directly from localStorage

    // console.log("Setting interceptors!!");
    const token = localStorage.getItem("token");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    } else {
      // if token doesn't exists... always create a new user? lol what
      const res = await fetch("http://localhost:3001/api/accounts", {
        method: "POST",
      });
      const data = await res.json();

      const token = data.token;
      const user_id = data.user._id;

      // console.log("token, user", data, token, user, res);

      localStorage.setItem("token", token);
      localStorage.setItem("user_id", user_id);
    }

    return config;
  });

  // return axios;
  //   }, []);

  return (
    <AxiosContext.Provider value={axios}>{children}</AxiosContext.Provider>
  );
}

export function useAxios() {
  return useContext(AxiosContext);
}
