import { useQuery, useMutation, useQueryClient } from "react-query";
import { useAxios } from "../providers/axiosProvider";
import { useAsker } from "../providers/askerProvider";

export function useUpdateSchedule(onSuccess) {
  // const asker = useAsker(); // its like call set state open modal
  const axios = useAxios();
  const user_id = localStorage.getItem("user_id");
  const queryClient = useQueryClient();

  return useMutation(
    (updateBody) =>
      axios.put(`http://localhost:3001/api/schedules/${user_id}`, updateBody),
    {
      onMutate: (variables) => {
        return { id: 1 };
      },
      // onError: (error) => console.log("error in mutate", error.response),
      onSuccess: (data, variables, context) => {
        // data should be a single activity (maybe inside array [])
        // query activities expects user_id... also is an array of all activities
        // console.log("On success is called after mutation is succesful");

        // console.log(
        //   "after schedule succesfully created",
        //   data,
        //   variables,
        //   context
        // );

        queryClient.setQueryData(
          [
            "schedules",
            {
              user_id: data.data.schedule.user_id,
              dateX: new Date(data.data.schedule.date).toDateString(),
            },
          ],
          { schedule: [data.data.schedule] }
        );

        if (onSuccess) {
          onSuccess();
        }

        // closeModal();
      },
    }
  );
}

export function useDeleteSchedule(date) {
  //   const asker = useAsker(); // its like call set state open modal
  const axios = useAxios();
  const user_id = localStorage.getItem("user_id");
  const queryClient = useQueryClient();

  return useMutation(
    (deleteBody) => {
      // console.log("delete body", deleteBody);
      return axios.delete(
        `http://localhost:3001/api/schedules/${user_id}?date=${new Date(date)}`,
        { data: deleteBody }
      );
    },
    {
      onMutate: (variables) => {
        return { id: 1 };
      },
      onSuccess: (data, variables, context) => {
        // data should be a single activity (maybe inside array [])
        // query activities expects user_id... also is an array of all activities
        // console.log("On success is called after mutation is succesful");

        // console.log(
        //   "after schedule succesfully deleted",
        //   data,
        //   variables,
        //   context
        // );

        queryClient.setQueryData(
          [
            "schedules",
            {
              user_id: data.data.schedule.user_id,
              dateX: new Date(data.data.schedule.date).toDateString(),
            },
          ],
          { schedule: [data.data.schedule] }
        );

        // closeModal();
      },
    }
  );
}

export function usePostSchedule() {
  const asker = useAsker(); // its like call set state open modal
  const axios = useAxios();
  const user_id = localStorage.getItem("user_id");
  const queryClient = useQueryClient();

  return useMutation(
    (date) =>
      axios.post(
        `http://localhost:3001/api/schedules/${user_id}?date=${new Date(date)}`
      ),
    {
      onMutate: (variables) => {
        return { id: 1 };
      },
      onSuccess: (data, variables, context) => {
        // data should be a single activity (maybe inside array [])
        // query activities expects user_id... also is an array of all activities
        // console.log("On success is called after mutation is succesful");

        const returnedSchedules = data.data.schedule;

        console.log(
          "after schedule succesfully posted",
          // data,
          returnedSchedules
          // variables,
          // context
        );
        returnedSchedules.forEach((schedule) => {
          queryClient.setQueryData(
            [
              "schedules",
              {
                user_id: user_id,
                dateX: new Date(schedule.date).toDateString(),
              },
            ],
            { schedule: [schedule] }
          );
        });

        // onPostSuccess();

        // refetchSchedule();

        // closeModal();
      },
    }
  );
}

export function useGetSchedule(date) {
  const asker = useAsker(); // its like call set state open modal
  const axios = useAxios();
  const user_id = localStorage.getItem("user_id");
  // const queryClient = useQueryClient();

  return useQuery(
    ["schedules", { user_id, dateX: new Date(date).toDateString() }],
    async ({ queryKey }) => {
      //   console.log("date inside use query", new Date(queryKey[1].dateX));
      const { data } = await axios.get(
        `http://localhost:3001/api/schedules/${queryKey[1].user_id}/?date=${queryKey[1].dateX}`
      );

      return data;
    },
    {
      // keepPreviousData: false,
      refetchOnMount: false,
      refetchOnWindowFocus: false,
      retry: false,
      enabled: false,
      // refetchOnMount: false,
      // refetchOnWindowFocus: false,
      // enabled: shouldFetch,
      onSuccess: (data) => {
        // console.log("ON SUCCESS SCHEDULES GET", data);
        if (data.schedule.length > 0) {
          data.schedule[0].schedule.forEach((act) => {
            if (
              act.committed &&
              act.time_has_passed &&
              act.status === "UNDETERMINED"
            ) {
              // console.log(
              //   "HAVE YOU DONE THIS ACTIVITY? IN ON SUCCESS",
              //   act.activity_name
              // );
              asker.push({
                ...act,
                schedule_id: data.schedule[0]._id,
              });
              // add to a list that show modal if items in array
            } else {
              // console.log(
              //   "KEEP DOING THIS ACTIVITY! IN ON SUCCESS",
              //   act.activity_name
              // );
            }
          });
        }
      },
    }
  );
}
