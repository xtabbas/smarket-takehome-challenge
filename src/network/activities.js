import { useQuery, useMutation, useQueryClient } from "react-query";
import { useAxios } from "../providers/axiosProvider";

export function useGetActivities(setCreateActivity) {
  const axios = useAxios();
  const user_id = localStorage.getItem("user_id");
  const queryClient = useQueryClient();
  return useQuery(
    ["activities", user_id],
    async ({ queryKey }) => {
      const { data } = await axios.get(
        `http://localhost:3001/api/activities/${queryKey[1]}`
      );

      return data;
    },
    {
      refetchOnMount: false,
      refetchOnWindowFocus: false,
      retry: false,
      enabled: false,
      onSuccess: (stff) => {
        // console.log("error after fetching activities", stff.response);
      },
      onError: (error) => {
        // console.log("error after fetching activities", error.response);
        if (error.response.status === 422) {
          if (setCreateActivity) {
            setCreateActivity({ modalIsOpen: true });
          }
        }
      },
    }
  );
}

export function useUpdateActivities() {
  const axios = useAxios();
  const user_id = localStorage.getItem("user_id");
  const queryClient = useQueryClient();
  return useMutation(
    (updatesActivity) =>
      axios.put(
        `http://localhost:3001/api/activities/${user_id}?activity_id=${updatesActivity._id}`,
        updatesActivity.activity
      ),
    {
      onMutate: (variables) => {
        return { id: 1 };
      },
      onSuccess: (data, variables, context) => {
        // data should be a single activity (maybe inside array [])
        // query activities expects user_id... also is an array of all activities
        // console.log("On success is called after mutation is succesful");

        // console.log(
        //   "after activtiy succesfully created",
        //   data,
        //   variables,
        //   context
        // );
        queryClient.setQueryData(
          ["activities", data.data.activity.user_id],
          [data.data.activity]
        );

        // closeModal();
      },
    }
  );
}

export function usePostActivities(postSchedule, closeModal) {
  const axios = useAxios();
  const user_id = localStorage.getItem("user_id");
  const queryClient = useQueryClient();
  return useMutation(
    (newActivity) =>
      axios.post(
        `http://localhost:3001/api/activities/${user_id}`,
        newActivity
      ),
    {
      onMutate: (variables) => {
        return { id: 1 };
      },
      onSuccess: (data, variables, context) => {
        // data should be a single activity (maybe inside array [])
        // query activities expects user_id... also is an array of all activities
        // console.log("On success is called after mutation is succesful");

        // console.log(
        //   "after activtiy succesfully created",
        //   data,
        //   variables,
        //   context
        // );
        queryClient.setQueryData(
          ["activities", data.data.activity.user_id],
          [data.data.activity]
        );

        postSchedule(new Date());

        closeModal();
      },
    }
  );
}
