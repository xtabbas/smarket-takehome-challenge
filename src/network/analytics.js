import { useQuery, useMutation, useQueryClient } from "react-query";
import { useAxios } from "../providers/axiosProvider";

export function useGetAnalytics(activity_id) {
  const axios = useAxios();
  const user_id = localStorage.getItem("user_id");
  const queryClient = useQueryClient();
  return useQuery(
    ["analytics", { user_id, activity_id }],
    async ({ queryKey }) => {
      const { data } = await axios.get(
        `http://localhost:3001/api/analytics/${queryKey[1].user_id}/?activity_id=${queryKey[1].activity_id}`
      );

      return data;
    },
    {
      refetchOnMount: false,
      refetchOnWindowFocus: false,
      enabled: false,
      retry: false,
    }
  );
}
