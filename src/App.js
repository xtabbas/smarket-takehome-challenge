import React, { useState, useEffect } from "react";
// import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Modal from "react-modal";
import { useGetActivities } from "./network/activities";
import Schedule from "./components/Schedule";
import CreateActivityModal from "./components/CreateActivityModal";
import { usePostSchedule } from "./network/schedules";
const hoursStepper = (steps, time) => {
  const timeString = new Date(time).getTime();
  const plusStepHours = timeString + steps * 60 * 60 * 1000;

  return new Date(plusStepHours);
};
const today = new Date();

const customStyles = {
  content: {
    // top: "50%",
    // left: "50%",
    // right: "auto",
    // bottom: "auto",
    // marginRight: "-50%",
    // transform: "translate(-50%, -50%)",

    boxSizing: "border-box",
    width: "760px",
    // boxSizing: "0em",
    maxWidth: "100%",
    margin: "auto",
    padding: "24px",
    borderTopLeftRadius: "16px",
    borderTopRightRadius: "16px",
    borderBottomRightRadius: "16px",
    borderBottomLeftRadius: "16px",
    /* border: none, */
    // background: "#000",
    backgroundColor: "hsl(209, 35%, 11.5%)",
    /* box-shadow: none, */
    position: "relative",
    flex: "0 1 auto",
  },
  overlay: {
    position: "fixed",
    top: "0px",
    left: "0px",
    width: "100%",
    height: "100%",
    background: "hsl(209, 35%, 11.5%, 70%)",
    overflowY: "scroll",
    zIndex: "11",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: "18px",
    boxSizing: "border-box",
    pointerEvents: "unset",
    opacity: "1",
    transform: "none",
    transformOrigin: "50% 50% 0px",
  },
};

Modal.setAppElement("#root");

function App() {
  let subtitle;
  const [createActivity, setCreateActivity] = React.useState({
    modalIsOpen: false,
    activityToEdit: {},
    edit: false,
  });

  const [active, setActive] = useState(new Date().toDateString());
  const y = [
    hoursStepper(24 * -2, today),
    hoursStepper(24 * -1, today),
    today,
    hoursStepper(24 * 1, today),
    hoursStepper(24 * 2, today),
  ];

  const [datesArray, setDatesArray] = useState(y);

  const {
    data: userActivities,
    status,
    error,
    refetch,
  } = useGetActivities(setCreateActivity);

  useEffect(() => {
    refetch();
  }, [refetch]);

  const { mutate: postSchedule, isLoading: isPostingSchedule } =
    usePostSchedule();

  // console.log("createActivity", createActivity);

  return (
    <div className="App">
      {/* navigation bar at the top */}
      <div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>User name</div>

          <div style={{ display: "flex" }}>
            <div style={{ margin: 2 }}>activities</div>
            <div style={{ margin: 2 }}>schedule</div>
            <div style={{ margin: 2 }}>analytics</div>
          </div>

          <div style={{ display: "flex" }}>
            <div>upgrade</div>
            <div>settings</div>
          </div>
        </div>
        <br />
      </div>

      <h1 style={{ fontSize: 26 }}>Schedule for July 29 2021</h1>

      {/* canvas */}
      <div className="canvas">
        <div style={{ position: "absolute", left: 0, bottom: "48%" }}>
          <button
            onClick={() => {
              datesArray.pop();
              const s = datesArray.reduce((acc, val, i, arr) => {
                if (i === 0) {
                  return [hoursStepper(24 * -1, val), val];
                } else {
                  return [...acc, val];
                }
              }, []);

              setDatesArray(s);
            }}
          >
            {"<"}
          </button>
          {datesArray[2].toDateString() !== new Date().toDateString() ? (
            <button
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                setDatesArray(y);
              }}
            >
              today
            </button>
          ) : null}
        </div>

        <div
          className="days"
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {datesArray.map((x) => {
            const isToday = x.toDateString() === new Date().toDateString();
            // console.log(x.toDateString(), new Date().toDateString(), isToday);
            // const isToday = active.getDate() === x.getDate();
            return (
              <Schedule
                key={x}
                date={x}
                isToday={isToday}
                activities={userActivities}
                active={
                  datesArray[2].toDateString() === new Date(x).toDateString()
                }
                postSchedule={postSchedule}
                isPostingSchedule={isPostingSchedule}
                setCreateActivity={(values) =>
                  setCreateActivity({ ...createActivity, ...values })
                }
              >
                {x.toDateString()}
              </Schedule>
            );
          })}
          {/* <div class="schedule"></div> */}
          {/* <div class="" style={active && activeStyle}></div> */}
          {/* <div class="schedule"></div> */}
          {/* <div class="schedule"></div> */}
        </div>

        <button
          style={{ position: "absolute", right: 0, bottom: "48%" }}
          onClick={() => {
            const s = datesArray.reduce((acc, val, i, arr) => {
              if (i === 0) {
                return acc;
              } else {
                return [...acc, val];
              }
            }, []);
            setDatesArray([
              ...s,
              hoursStepper(24 * 1, datesArray[datesArray.length - 1]),
            ]);
          }}
        >
          {">"}
        </button>
      </div>

      <button style={{ position: "absolute", bottom: 0, left: "48%" }}>
        add activity
      </button>

      <Modal
        isOpen={createActivity.modalIsOpen}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <CreateActivityModal
          innerRef={(_subtitle) => (subtitle = _subtitle)}
          closeModal={() =>
            setCreateActivity({ ...createActivity, modalIsOpen: false })
          }
          subtitle={subtitle}
          postSchedule={postSchedule}
          activityToEdit={createActivity.activityToEdit}
          edit={createActivity.edit}
        />
      </Modal>
    </div>
  );
}

export default App;
