module.exports = {
  date: new Date(),
  utc: function (passedDate) {
    // converts to utc and returns self
    // console.log(passedDate);

    // if (!passedDate) {
    //   throw "wait what";
    // }

    this.date = passedDate
      ? new Date(passedDate.toUTCString())
      : new Date(this.date.toUTCString());
    return this;
  },
  onlyDate: function () {
    this.date = new Date(new Date(this.date).setHours(1, 0, 0, 0));
    return this;
  },
  setTimeFrom: function (passedDate) {
    const x = new Date(this.date);
    const y = new Date(passedDate);

    this.date = new Date(
      x.setHours(
        y.getHours(),
        y.getMinutes(),
        y.getSeconds(),
        y.getMilliseconds()
      )
    );
    return this;
  },
  minus: function (from) {
    const days = Math.ceil(
      (new Date(this.date) - new Date(from)) / (1000 * 60 * 60 * 24)
    );
    return days;
  },
};
