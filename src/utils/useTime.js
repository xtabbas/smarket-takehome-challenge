// copied from daybridge; don't tell them or we get sued
function useTimeDelta() {}
function cononicalTimeZoneFor() {}
// function useState() {}
// function useEffect() {}
function useInterval() {}
// function useCallback() {}
class DateTime {}

module.exports = function useDate(notifyInterval, timeZone) {
  // The delta tells us how far out of sync the users computer is from
  // the server. This needs to be subtracted from whatever time the user's
  // computer thinks it is in order to obtain an accurate time.
  const delta = useTimeDelta();

  // `currectTime` is a function that returns the current correct time
  // in the requested time zone with the right delta applied
  const correctTime = useCallback((delta, timeZone) => {
    let time = DateTime.utc().minus({ milliseconds: delta });
    if (timeZone) {
      time = time.setZone(cononicalTimeZoneFor(timeZone));
    }
    return time;
  }, []);

  // `timeSnapshot` keeps a snapshot of the time for the caller. Initizlised
  // by default to the corrected time. It will stay at this value until either
  // the delta changes, or the notify interval is hit.
  const [timeSnapshot, setTimeSnapshot] = useState(
    correctTime(delta, timeZone)
  );

  // `tick` refreshes the current time
  const tick = useCallback(
    () => setTimeSnapshot(correctTime(delta, timeZone)),
    [delta, timeZone]
  );

  // If the delta or timezone change, refresh the time.
  useEffect(() => tick(), [delta, timeZone]);

  // Refresh the time at a regular inteval if that's what the caller has requested.
  // Note that this hook can handle `undefined` notifyIntervals fine.
  useInterval(tick, notifyInterval);

  return timeSnapshot;
};
