import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { AxiosProvider } from "./providers/axiosProvider";
import { ActivitiesProvider } from "./providers/activitiesProvider";
import { AskerProvider } from "./providers/askerProvider";

import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";

const queryClient = new QueryClient();

ReactDOM.render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <AxiosProvider>
        <AskerProvider>
          {/* <ActivitiesProvider> */}
          <App />
          <ReactQueryDevtools initialIsOpen={false} />
          {/* </ActivitiesProvider> */}
        </AskerProvider>
      </AxiosProvider>
    </QueryClientProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
