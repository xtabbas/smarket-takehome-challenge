module.exports = {
  root: true,
  extends: "@react-native-community",
  rules: {
    "prettier/prettier": ["error", { singleQuote: false, parser: "flow" }],
    "arrow-body-style": "off",
    "prefer-arrow-callback": "off",
    "comma-dangle": "off",
    quotes: [2, "double", { avoidEscape: true }],
  },
};
